const express = require("express")
const app = express()
const fs = require("fs")

app.get("/books", (req,res)=> {

 fs.readFile(process.argv[3], (err,val)=>{

 	if(err) res.sendStatus(400)

 	 res.json(JSON.parse(val))
 })

})

app.listen(process.argv[2])